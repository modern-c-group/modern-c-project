#include <QtTest>

// add necessary includes here
#include "C:/Users/Octavian/Desktop/Mosaic++/MosaicQT/mainwindow.h"
#include "C:/Users/Octavian/Desktop/Mosaic++/MosaicQT/mainwindow.cpp"


class TestMosaic : public QMainWindow
{
    Q_OBJECT

public:
    TestMosaic();
    ~TestMosaic();
private:
    MainWindow m;

private slots:
    void test_case1();
    void TestXY();

};

TestMosaic::TestMosaic()
{

}

TestMosaic::~TestMosaic()
{

}

void TestMosaic::test_case1()
{

}

void TestMosaic::TestXY()
{
    QVERIFY2(m.x==m.getX(), "Var x not set");
    QVERIFY2(m.y==m.getY(), "Var y not set");
}

QTEST_APPLESS_MAIN(TestMosaic)

#include "tst_testmosaic.moc"
